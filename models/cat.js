const Sequelize = require('sequelize');
const sequelize = require('../util/database');
const Cat = sequelize.define('cat', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    },
    age: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    breed: {
        type: Sequelize.STRING,
        allowNull: false
    }
})
module.exports = Cat;