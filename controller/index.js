// RECCOMENDED TO USE "JSON VIEWER" EXTENSION INSIDE CHROME
// Used Postman for API Testing
const Cat = require('../models/cat');
const Op = require('sequelize').Op;
exports.listCats = (req, res) => {
    Cat.findAll()
        .then(result => res.status(200).send(result))
        .catch(err => {
            res.status(404).send("Cats not found");
            throw err;
        })
}
exports.getCat = (req, res) => {
    if (!req.params.id) return res.status(400).send("Id not valid");
    Cat.findAll({ where: { id: req.params.id } })
        .then(result => res.status(200).send(result))
        .catch(err => {
            res.status(404).send("Cat not found");
            res.redirect('/');
            throw err;
        })
}
exports.searchCat = (req, res) => {
    const allowedQuery = ['age_lte', 'age_gte'];
    if (Object.keys(req.query).length < 2) return res.status(400).send("Incomplete query");
    Object.keys(req.query).forEach((query) => {
        if (!allowedQuery.includes(query)) {
            return res.status(400).send("Invalid query");
        }
    });
    const { age_lte, age_gte } = req.query;
    Cat.findAll({ where: { age: { [Op.gt]: age_gte, [Op.lt]: age_lte } } })
        .then(result => res.status(200).send(result))
        .catch(err => {
            console.log(err);
            res.status(404).send();
            throw err;
        })
}
exports.editCat = (req, res) => {
    Cat.update({ ...req.body }, { where: { id: req.params.id } })
        .then(() => res.status(200).send("Successful"))
        .catch(err => {
            res.status(404).send("Some error occured");
            throw err
        });
}
exports.addCat = (req, res) => {
    const { name, age, breed } = req.body;
    if (!name || !age || !breed) return res.status(400).send("Enter name, age & breed");
    Cat.create({
        name, age, breed
    }).then((result) => res.status(200).send(result.dataValues))
        .catch(err => {
            res.status(400).send("Some error occured");
            throw err;
        });
}
exports.deleteCat = (req, res) => {
    Cat.destroy({ where: { id: req.params.id } })
        .then(() => res.status(200).send("Successful"))
        .catch(err => {
            res.status(404).send("Some error occured");
            throw err;
        })
}