const express = require('express');
const app = express();
const appRoutes = require('./routes');
const jsonParser = express.json();
const urlencodedParser = express.urlencoded({ extended: false });
app.use(urlencodedParser);
app.use(jsonParser);
const sequelize = require('./util/database');

app.use('', appRoutes);
// app.use('/', (req, res) => {
//     res.redirect('/list');
// })
sequelize.sync()
    .then(() => {
        app.listen(3000);
    })
    .catch(err => { throw err })